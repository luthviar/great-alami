import React, {memo} from 'react';
import { View, Text, Pressable, StyleSheet } from 'react-native';

const QuantitySelector = React.memo( ({quantity, onPlusQuantity, onMinusQuantity}) => {        
    return (
      <View style={styles.root}>
        <Pressable onPress={onMinusQuantity} style={styles.button}>
          <Text style={styles.butonText}>-</Text>
        </Pressable>
  
        <Text style={styles.quantity}>
          {quantity}
        </Text>
  
        <Pressable onPress={onPlusQuantity} style={styles.button}>
          <Text style={styles.butonText}>+</Text>
        </Pressable>
      </View>
    );
});

const styles = StyleSheet.create({
  root: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    borderWidth: 1,
    borderColor: '#e3e3e3',
    width: 130,
  },
  button: {
    width: 35,
    height: 35,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#1c8aff',
  },
  butonText: {
    fontSize: 18,
  },
  quantity: {
    color: '#007eb9',
  },
});

export default memo(QuantitySelector);