/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React, { useState } from 'react';
import { Button, StyleSheet, Text, SafeAreaView, View, TouchableOpacity } from 'react-native';
import DeviceInfo from './pages/DeviceInfo';
import ProductScreen from './pages/ProductScreen';
import ProgressBar from './pages/ProgressBar';
import { NavigationContainer, StackActions } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';


function HomeScreen({ navigation }) {
  return (
    <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
      <Text style={styles.instructions} >Home Screen</Text>
      <View style={styles.buttonContainer}>
        <Button          
          title="Go to Device Info Page"
          onPress={() => {
            navigation.navigate('DeviceInfo', {
              title: "React Native Device Info"
            });
          }}
        />
      </View>      
      <View style={styles.buttonContainer}>
        <Button          
          title="Go to Add To Cart Page"
          onPress={() => {
            navigation.navigate('ProductScreen', {
              title: "Add To Cart"
            });
          }}
        />
      </View>     
      <View style={styles.buttonContainer}>
        <Button
          title="Go to Progress Bar Page"
          onPress={() => {
            navigation.navigate('ProgressBar', {
              title: "Progress Bar Page"
            });
          }}
        />
      </View>       
    </View>
  );
}


const App = () => {
  const [activeTab, setActiveTab] = useState('infoOne');
  const Stack = createNativeStackNavigator();
  
  return (
    <NavigationContainer>
      <Stack.Navigator initialRouteName="Home">                
        <Stack.Screen name="Home" component={HomeScreen} />
        <Stack.Screen name="DeviceInfo" component={DeviceInfo}/>        
        <Stack.Screen name="ProductScreen" component={ProductScreen}/>        
        <Stack.Screen name="ProgressBar" component={ProgressBar}/>
      </Stack.Navigator>    
    </NavigationContainer>    
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#F5FCFF',
  },
  buttonContainer: {
    margin: 10
  },
  instructions: {
      textAlign: 'left',
      color: 'black',
      margin: 5
  }
})

export default App
