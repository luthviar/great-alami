# Great Alami

- This is source code for alami task
- This repository is using React Native version 0.68.2 (https://github.com/facebook/react-native/releases/tag/v0.68.2)
- 


# Getting started - How To Install & Run the App

## Prerequisites
1. install npm 
2. install react-native cli: https://reactnative.dev/docs/environment-setup
3. install Android Studio (For Android SDK and Android Emulator and also Android Real device if needed)
4. install XCode (For iOS App Development purpose)

## Running the App
1. git clone https://gitlab.com/luthviar/great-alami.git
2. open terminal within the cloned directory, then run: npm install
3. run this on terminal to run on android device: npx react-native run-android
4. run this on terminal to run on iOS device: npx react-native run-ios
5. On the home screen, there are 3 button to navigate: "Go to Device Info Page", "Go to Add To Cart Page", "Go to Progress Bar Page"

# Demo Android & iOS

## Demo Android
![Attatched Screenshot](/assets_demo/android_demo.gif)

## Demo iOS
![Attatched Screenshot](/assets_demo/ios_demo.gif)