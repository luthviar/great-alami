import React, {useState, useEffect} from 'react';
import { StyleSheet, Button, View, Text, TouchableOpacity } from 'react-native';
import * as Progress from 'react-native-progress';


function ProgressBar({ navigation }) {
    const [count, setCount] = useState(0);
    var id = 0

    useEffect(() => {
        
        id = setInterval(() => {
                if(count >= 10) {
                    clearInterval(id)
                } else {
                    setCount((oldCount) => oldCount + 0.5)
                }
            },            
            1000
        );
                
      return () => {
        clearInterval(id);
      };
    }, [count]);    
  
    function onPressInProgressBar() {        
        console.log("onPressInProgressBar")
        clearInterval(id)                
    }

    function onPressOutProgressBar() {
        console.log("onPressOutProgressBar")
        if(count < 10) { setCount(count + 0.5) }        
    }

    function onPressRestartProgressBar() {
        console.log("onPressRestartProgressBar")
        setCount(0)
    }
    
    return (
        <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
            <Text style={{margin: 20, textAlign: 'center', color: 'black'}}>Progress Bar is Auto Start. Touch and hold the progrss bar to pause, and release the touch to continue the progress.</Text>
            <View style={{flex: 1, justifyContent: "flex-end"}}>                
            <Text style={{textAlign:'center', color:'black'}}>{count*10}%</Text>                    
                <TouchableOpacity onPressIn={onPressInProgressBar} onPressOut={onPressOutProgressBar}>
                    <>
                        <Progress.Bar progress={count/10} borderRadius={20} height={30} width={300} />
                    </>
                </TouchableOpacity>                
            </View>        
            <View style={{flex: 2, justifyContent: "flex-end", marginBottom: 50}}>
                <View style={{margin:10}}>
                    <Button title="Restart Progress Bar" onPress={onPressRestartProgressBar} />                
                </View>
                <View style={{margin:10}}>
                    <Button title="Go back" onPress={() => navigation.goBack()} />
                </View>                
            </View>            
        </View>
    );
}

const styles = StyleSheet.create({
    titleStyle: {
        fontSize: 20,
        textAlign: 'center',
        margin: 10
    },
    instructions: {
        textAlign: 'left',
        color: 'black',
        margin: 5
    }
}); 

export default ProgressBar