import React from 'react'
import { StyleSheet, Text, ScrollView, Button } from 'react-native' 
import { getUniqueId, getDeviceId, useBatteryLevel, useBatteryLevelIsLow, usePowerState, useFirstInstallTime, useDeviceName, useIsEmulator} from 'react-native-device-info'

const DeviceInfo = ({route, navigation}) => {
    const { title } = route.params;    
    let deviceJSON = {}; 
    deviceJSON.uniqueId = getUniqueId();
    deviceJSON.getDeviceId = getDeviceId();
    deviceJSON.batteryLevel = useBatteryLevel(); 
    deviceJSON.batteryLevelIsLow = useBatteryLevelIsLow();
    deviceJSON.powerState = usePowerState();
    deviceJSON.firstInstallTime = useFirstInstallTime();
    deviceJSON.deviceName = useDeviceName(); 
    deviceJSON.isEmulator = useIsEmulator(); 
    return(
        <>
        <Text style={styles.titleStyle}>{title}</Text> 
            <ScrollView> 
                <Text style={styles.instructions}>
                    {JSON.stringify (deviceJSON, null,' ')} 
                </Text> 
                <Button title="Go back" onPress={() => navigation.goBack()} />
            </ScrollView>
        </>
    )    
}
    
const styles = StyleSheet.create({
    titleStyle: {
        fontSize: 20,
        textAlign: 'center',
        margin: 10,
        color: 'black'
    },
    instructions: {
        textAlign: 'left',
        color: 'black',
        margin: 5
    }
}); 
export default DeviceInfo