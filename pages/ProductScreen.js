import React, {useState, useReducer} from 'react';
import { StyleSheet, Button, View, Text, Image } from 'react-native';
import QuantitySelector from '../components/QuantitySelector';
import "intl";
import "intl/locale-data/jsonp/en";


function ProductScreen({ navigation }) {
  
    function reducer(state, action) {    
      switch (action.type) {
        case 'increment':
          return ({ ...state, count: state.count + 1 })
        case 'decrement':          
          return ({ ...state, count: Math.max(0, state.count - 1) })      
      }
    }

    const [counterCart, setCounterCart] = useState(0)    
    const [state, dispatch] = useReducer(reducer, { count: 1 })
    const [price, setPrice] = useState(115000);
    const [totalPrice, setTotalPrice] = useState(0);

    const addToCart = () => {
      if(state.count > 0) {
        setCounterCart(counterCart + state.count);                
        setTotalPrice(price * (counterCart+state.count));
      }        
    }

    const emptyCart = () => {      
      setCounterCart(0);                
      setTotalPrice(0);      
    }
    

    const rupiah = (number) => {
      return new Intl.NumberFormat("id-ID", {
        style: "currency",
        currency: "IDR",
        minimumFractionDigits: 0 
      }).format(number);
    }

    return (                   
        <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center', color: "black" }}>
        
        <Text style={styles.instructions} >Total Item(s) on Cart: {counterCart} </Text>                   
        <Text style={styles.instructionsOther} >Total Prices on Cart: {rupiah(totalPrice)} </Text>           
        <View style={{marginBottom:45, marginTop:-20, fontSize: 5}}>
          <Button titleStyle={{
                    color: "white",
                    fontSize: 16,
                }}
                buttonStyle={{
                    backgroundColor: "white",
                    borderRadius: 60,
                    flex: 1,
                    height: 30,
                    width: 30,  
                }} title='Empty Cart' onPress={() => emptyCart()} />        
        </View>

        <Image source={{uri:"https://learn-quran.co/iosapp/productimage.jpeg", width:200,height:200 }}/>
        <Text style={{fontSize:30, fontWeight: "bold", color: "black"}}>{rupiah(price)}</Text>
        <Text style={{paddingLeft:40, paddingRight:40, marginTop:10, color: "black", marginBottom:20}}>PowerBank ROBOT 10000mAh RT180 Dual Input Port Type C & Micro USB - white</Text>        
        <QuantitySelector quantity={state.count} onPlusQuantity={() => dispatch({ type: "increment" })} onMinusQuantity={() => dispatch({ type: "decrement" })} />                
        <View style={{marginBottom:50, marginTop:20}}>
          <Button title='Add to Cart' onPress={() => addToCart()} />        
        </View>
        <Button title="Go back" onPress={() => navigation.goBack()} />

        </View>
    );
}

const styles = StyleSheet.create({
    titleStyle: {
        fontSize: 20,
        textAlign: 'center',
        margin: 10
    },
    instructions: {
        textAlign: 'left',
        color: 'black',
        margin: 20
    },
    instructionsOther: {
      textAlign: 'left',
      color: 'black',
      marginBottom: 30
  }
}); 

export default ProductScreen